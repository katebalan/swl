swl
===

* Date          September 18, 2018
* Symfony:      3.4.15
* PHP:          7.1.x
* dependencies: Composer

### To install project:

* ```git clone git@github.com:katebalan/swl.git```
* ```cd swl``` - enter to project's folder
* ```composer install```
A file app/config/parameters.yml must be auto-generated during the composer install.
Make sure that you fill it correct:
``` app/config/parameters.yml
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: swl
    database_user: root
    database_password: null
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: ThisTokenIsNotSoSecretChangeIt

```
* ```./bin/console doctrine:database:create``` - to create database
* ```./bin/console doctrine:migrations:migrate``` - to make migrations
* ```./bin/console doctrine:fixtures:load``` - to use fixtures
* ```./bin/console server:start``` - to start build-in server
* open in browser http://127.0.0.1:8000/

**or you can use script for installation:**

* ```git clone git@github.com:katebalan/swl.git```
* ```cd swl``` - enter to project's folder
* ```./scripts/start.sh``` - to run script (don't forget to fill your database_user and database_password)
* open in browser http://127.0.0.1:8000/


if you have some problems with cache, logs or sessions, you can use:
```chmod 777 -R var/cache var/sessions var/logs```

in db/ folder you can find dump of test database.
