<?php

namespace SwivlClassroomBundle\Controller;

use SwivlClassroomBundle\Entity\Classroom;
use SwivlClassroomBundle\Form\ClassroomFormType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ClassroomController
 * @package SwivlClassroomBundle\Controller
 *
 * @Route("/classroom")
 */
class ClassroomController extends Controller
{
    /**
     * Controller are used for show list of all classrooms
     *
     * @return array
     * @Route("/", name="classroom_list")
     * @Template()
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $classrooms = $em->getRepository('SwivlClassroomBundle:Classroom')->findAll();

        return [
            'classrooms' => $classrooms
        ];
    }

    /**
     * Controller are used to create new classroom
     *
     * @param Request $request
     * @return array|RedirectResponse
     * @Route("/new", name="classroom_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(ClassroomFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $classroom = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($classroom);
            $em->flush();

            $this->addFlash('success', 'New classroom is created!');

            return $this->redirectToRoute('classroom_show', ['id' => $classroom->getId()]);
        }
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Classroom $classroom
     * @return array
     * @Route("/{id}", name="classroom_show")
     * @Template()
     */
    public function showAction(?Classroom $classroom)
    {
        if (!$classroom) {
            throw $this->createNotFoundException('The classroom does not exist');
        }

        return [
            'classroom' => $classroom
        ];
    }

    /**
     * @param Request $request
     * @param Classroom|null $classroom
     * @return array|RedirectResponse
     * @Route("/{id}/edit", name="classroom_edit")
     * @Template()
     */
    public function editAction(Request $request, ?Classroom $classroom)
    {
        if (!$classroom) {
            throw $this->createNotFoundException('The classroom does not exist');
        }

        $form = $this->createForm(ClassroomFormType::class, $classroom);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $classroom = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($classroom);
            $em->flush();

            $this->addFlash('success', 'Classroom is updated!');

            return $this->redirectToRoute('classroom_show', ['id' => $classroom->getId()]);
        }

        return [
            'form' => $form->createView(),
            'id' => $classroom->getId(),
        ];
    }

    /**
     * @param Classroom|null $classroom
     * @return RedirectResponse
     * @Route("/{id}/delete", name="classroom_delete")
     */
    public function deleteAction(?Classroom $classroom)
    {
        if (!$classroom) {
            throw $this->createNotFoundException('The lassroom does not exist');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($classroom);
            $em->flush();

            $this->addFlash('success', 'Classroom is deleted!');
        }

        return $this->redirectToRoute('classroom_list');
    }

    /**
     * @Route("/list/update", name="ajax_classroom_list_update")
     */
    public function ajaxUpdateListAction()
    {
        $em = $this->getDoctrine()->getManager();
        if ((isset($_POST['showAll'])) && $_POST['showAll'] == 'false') {
            $classrooms = $em->getRepository('SwivlClassroomBundle:Classroom')->findAllActive();
        } else {
            $classrooms = $em->getRepository('SwivlClassroomBundle:Classroom')->findAll();
        }

        $jsonData = [];
        foreach ($classrooms as $classroom) {
            $jsonData[] = [
                'id' => $classroom->getId(),
                'name' => $classroom->getName(),
                'date' => $classroom->getDate()->format('Y-m-d'),
                'active' => ($classroom->isActive()) ? '1' : '0',
            ];
        }
        return new JsonResponse($jsonData);
    }
}
