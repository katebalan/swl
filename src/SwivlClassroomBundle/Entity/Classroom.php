<?php

namespace SwivlClassroomBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Products
 * @package SwivlClassroomBundle\Entity
 * @ORM\Table(name="classroom")
 * @ORM\Entity(repositoryClass="SwivlClassroomBundle\Repository\ClassroomRepository")
 */
class Classroom
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=false)
     * @Assert\NotBlank(
     *      message = "Name should not be blank."
     * )
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Name must be no longer than {{ limit }} characters."
     * )
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime", nullable=true, unique=false)
     * @Assert\DateTime(format="m/d/Y")
     * @Assert\DateTime(
     *     message="This date {{ value }} is not a valid date."
     * )
     */
    private $date;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean", nullable=true, unique=false)
     */
    private $active;

    /**
     * Classroom constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}
