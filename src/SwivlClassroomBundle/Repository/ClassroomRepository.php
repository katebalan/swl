<?php

namespace SwivlClassroomBundle\Repository;


use Doctrine\ORM\EntityRepository;
use SwivlClassroomBundle\Entity\Classroom;

class ClassroomRepository extends EntityRepository
{
    /**
     * @return Classroom
     */
    public function findAllActive()
    {
        return $this->createQueryBuilder('c')
            ->where('c.active = 1')
            ->getQuery()
            ->execute();
    }
}
